#[derive(PartialEq, Eq, Clone, Debug)]
pub struct ListNode {
  pub val: i32,
  pub next: Option<Box<ListNode>>
}

impl ListNode {
  #[inline]
  fn new(val: i32) -> Self {
    ListNode {
      next: None,
      val
    }
  }
  
  fn append(&mut self, val: i32) {
    let node = Box::new(ListNode::new(val));
    let mut cur = self;
    while cur.next.is_some() {
        cur = cur.next.as_mut().unwrap();
    } // get to the end of the list
    cur.next = Some(node);
  }

}

impl FromIterator<i32> for ListNode {
    fn from_iter<I: IntoIterator<Item=i32>>(iter: I) -> Self {
        let mut head = ListNode::new(0);
        for i in iter {
            head.append(i)
        }
        *head.next.unwrap()
    }
}



fn main () {
    let v = vec![1i32,2,3,4];
    let mut list = ListNode::from_iter(v);
    dbg!(list);
}
