# Morris Algorithm
[Morris Algorithm](https://www.sciencedirect.com/science/article/pii/0020019079900681) can do a Binary Search Tree in a very efficient way: *O(n)* for CPU and *O(1)* for memory.

The algorithm is the following, starting from root, iterate over these actions
- if there is no left branch, *yield root as next element* and update **root to root.right**
- if there is a left branch, find the predecessor (go left then all the way right) 
  - **if predecessor.right is None** link it to root **predecessor.right = root**
    - update *root to root.left*
  - **if predecessor.right = root** break the link
    - *yield root as next element*
    - update *root to root.right*
