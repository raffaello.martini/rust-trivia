#[derive(Debug, PartialEq, Eq)]
pub struct TreeNode {
  pub val: i32,
  pub left: Option<Rc<RefCell<TreeNode>>>,
  pub right: Option<Rc<RefCell<TreeNode>>>,
}

impl TreeNode {
  #[inline]
  pub fn new(val: i32) -> Self {
    TreeNode {
      val,
      left: None,
      right: None
    }
  }
}

use std::rc::Rc;
use std::cell::RefCell;
use std::collections::VecDeque;

fn build_tree(elem: &Vec<Option<i32>>) -> Option<Rc<RefCell<TreeNode>>> {
    let mut stack: VecDeque<Option<TreeNode>>;
    stack = elem.iter().
        map(|e| e.map( |e| TreeNode::new(e)))
        .collect(); // VecDeque<Option<TreeNode>>
    let root = stack.pop_front()
                .map(|cur| cur.map(
                    |cur| Rc::new(RefCell::new(cur))
                    )).unwrap();
    // let mut cur = root.clone();
    let mut stack2 = VecDeque::new();
    stack2.push_back(root.clone().unwrap());
    while let Some(cur) = stack2.pop_front() {
        if let Some(left) = stack.pop_front().flatten() {
            let left = Rc::new(RefCell::new(left));
            cur.borrow_mut().left = Some(left.clone());
            stack2.push_back(left);
        }
        if let Some(right) = stack.pop_front().flatten() {
            let right = Rc::new(RefCell::new(right));
            cur.borrow_mut().right = Some(right.clone());
            stack2.push_back(right);
        }
    }
    root
}

fn parse(s: &str) -> Vec<Option<i32>> {
    // s = [3,1,4,null,null,2]
    let s = &s[1..s.len()-1]; // strip the square brackets
    s.split(",").map( |e| e.parse::<i32>().ok()).collect()
}

fn main () {
    let input = "[1,3,null,null,2]";
    let v = parse(input);
    // let v = vec![Some(3),Some(1),Some(4),None,None,Some(2)];
    let root = build_tree(&v);
    dbg!(root);
}
