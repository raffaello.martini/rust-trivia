use std::mem::take;

#[derive(PartialEq, Eq, Clone, Debug)]
pub struct ListNode {
  pub val: i32,
  pub next: Option<Box<ListNode>>
}

impl ListNode {
  #[inline]
  fn new(val: i32) -> Self {
    ListNode {
      next: None,
      val
    }
  }
  
  fn append(&mut self, val: i32) {
    let node = Box::new(ListNode::new(val));
    let mut cur = self;
    while cur.next.is_some() {
        cur = cur.next.as_mut().unwrap();
    } // get to the end of the list
    cur.next = Some(node);
  }

}

impl FromIterator<i32> for ListNode {
    fn from_iter<I: IntoIterator<Item=i32>>(iter: I) -> Self {
        let mut head = ListNode::new(0);
        for i in iter {
            head.append(i)
        }
        *head.next.unwrap()
    }
}


pub fn swap_nodes(head: Option<Box<ListNode>>, k: i32) -> Option<Box<ListNode>> {
    let mut dummy = head?;
    for _ in 0..2 {
        let mut node = Box::new(ListNode::new(i32::MIN));
        node.next = Some(dummy);
        dummy = node;   
    }
    let mut n = 1;
    let mut cur = &mut dummy;
    let mut maybe_k_val = None;
    let mut maybe_k_next = None;
    let mut maybe_k_prev = None;
    while cur.next.is_some() {
        if n == k {
            let mut maybe_kth = cur.next.as_mut().unwrap().next.take();
            maybe_k_val = Some(maybe_kth.as_ref().unwrap().val);
            if maybe_kth.is_some() {
                maybe_k_next = maybe_kth.as_mut().unwrap().next.take();
                let k_prev = &mut cur.next;
                maybe_k_prev = Some(k_prev);
            }
            break;
        }
        cur = cur.next.as_mut().unwrap();
        n += 1;
    } 
    dbg!(&maybe_k_next);
    dbg!(&maybe_k_prev);
    dbg!(&maybe_k_val);
    
    // kth element taken off the list, prev element reference saved
    // dbg!(&k_prev_ref);
    // dbg!(&k_th);
    
    // let mut m_th = None;
    // let mut m_prev_ref = None;
    // let (mut cur1, mut cur2) = (dummy.next.as_mut().unwrap(), k_th.as_mut().unwrap());    
    // while cur2.next.is_some() {
    //     if cur1.next.is_some(){
    //         cur1 = cur1.next.as_mut().unwrap();
    //     } else {break;}
    //     cur2 = cur2.next.as_mut().unwrap();
    // }
    // dbg!(&cur1);
    // dbg!(&cur2);
    // cur2.next.is_some() {
    //     // bad case: m > k
    // } else {
    //     // easy case: m < k, cur1.next points to e_m
    //     if cur1.next.is_some() {
    //         let m_th = cur1.next.take();
    //         drop(cur2);
    //         cur1.next = k_th;
    //         k_prev_ref.unwrap().next = m_th;
    // }
    
    dummy.next.unwrap().next
}


fn main () {
    let v = vec![1i32,2,3,4,5];
    let mut head = Some(Box::new(ListNode::from_iter(v)));
    // dbg!(&head);
    swap_nodes(head, 1);
}

