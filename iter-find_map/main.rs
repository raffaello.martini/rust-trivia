fn main() {
    let nums: Vec<i32> = vec![1, 2, 3, 7, 5];
    let out = nums.iter()
    // .enumerate().
        .zip(nums[1..].iter())
        .enumerate()
        // .collect();
        .find_map(|(i,(n, m))| if n > m { Some(i) } else { None });
    assert_eq!(Some(3), out);

}
