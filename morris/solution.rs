// Definition for a binary tree node.
// #[derive(Debug, PartialEq, Eq)]
// pub struct TreeNode {
//   pub val: i32,
//   pub left: Option<Rc<RefCell<TreeNode>>>,
//   pub right: Option<Rc<RefCell<TreeNode>>>,
// }
// 
// impl TreeNode {
//   #[inline]
//   pub fn new(val: i32) -> Self {
//     TreeNode {
//       val,
//       left: None,
//       right: None
//     }
//   }
// }

use std::rc::Rc;
use std::cell::RefCell;
use std::mem;

impl Solution {
    pub fn recover_tree(root: &mut Option<Rc<RefCell<TreeNode>>>) {
        let mut helper = Helper::new();
        let mut root = root.clone(); // gets rid of &mut 
        while let Some(root_rc) = root.clone() {
            // find predecessor
            let e = { 
                if let Some(mut predecessor) = root_rc.borrow().left.clone() { // there is a left branch
                    // go to the predecessor
                    while predecessor.borrow().right.is_some() && predecessor.borrow().right != root {
                        let tmp = predecessor.borrow().right.clone().unwrap();
                        predecessor = tmp;
                    }
                    if predecessor.borrow().right.is_none() { 
                       // first time we have been here
                        predecessor.borrow_mut().right = Some(root_rc.clone());
                        root = root_rc.borrow().left.clone();
                        None
                    } else {
                        // predecessor.righ == root -> we've been here before
                        predecessor.borrow_mut().right = None;
                        root = root_rc.borrow().right.clone();
                        Some(root_rc.clone())
                    }
                } else  { // there is no left branch
                    root = root_rc.borrow().right.clone();
                    Some(root_rc.clone())
                }
            };
            if let Some(e) = e { // New item found in tree trasversal
                helper.add_transversal_elem(e);
            }
        }
        helper.swap();
    }
}

struct Helper{
    x: Option<Rc<RefCell<TreeNode>>>,
    y: Option<Rc<RefCell<TreeNode>>>,
    prev: Option<Rc<RefCell<TreeNode>>>,
    complete: bool
}

impl Helper{
    fn new() -> Helper {
        Helper {
            x: None,
            y: None,
            prev: None,
            complete: false,
        }
    }
    
    fn add_transversal_elem(&mut self, e: Rc<RefCell<TreeNode>>) {
        /// true if is completed
        /// false if is not completed
        if self.complete {
            return
        }
        let val = e.borrow().val;
        if let Some(prev) = self.prev.clone() {
            if val < prev.borrow().val {
                self.y = Some(e.clone());
                if self.x.is_some() {
                    self.complete = true;
                } else { // x is None
                    self.x = Some(prev.clone());
                }
            }
        }
        self.prev = Some(e.clone());
    }
    
    fn swap(&mut self){
        if let (Some(x), Some(y)) = (self.x.clone(), self.y.clone()) {
            mem::swap(&mut x.borrow_mut().val, &mut y.borrow_mut().val);
        }
    }
}
