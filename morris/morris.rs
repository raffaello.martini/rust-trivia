// Definition for a binary tree node.
#[derive(Debug, PartialEq, Eq)]
pub struct TreeNode {
  pub val: i32,
  pub left: Option<Rc<RefCell<TreeNode>>>,
  pub right: Option<Rc<RefCell<TreeNode>>>,
}

impl TreeNode {
  #[inline]
  pub fn new(val: i32) -> Self {
    TreeNode {
      val,
      left: None,
      right: None
    }
  }
}

fn morris(root: Option<Rc<RefCell<TreeNode>>>) {
    let mut root = root;
    while let Some(root_rc) = root.clone() {
        // find predecessor
        let e = { 
            if let Some(mut predecessor) = root_rc.borrow().left.clone() { // there is a left branch
                // go to the predecessor
                while predecessor.borrow().right.is_some() && predecessor.borrow().right != root {
                    let tmp = predecessor.borrow().right.clone().unwrap();
                    predecessor = tmp;
                }
                if predecessor.borrow().right.is_none() { 
                    // first time we have been here
                    predecessor.borrow_mut().right = Some(root_rc.clone());
                    root = root_rc.borrow().left.clone();
                    None
                } else {
                    // predecessor.righ == root -> we've been here before
                    predecessor.borrow_mut().right = None;
                    root = root_rc.borrow().right.clone();
                    Some(root_rc.clone())
                }
            } else  { // there is no left branch
                root = root_rc.borrow().right.clone();
                Some(root_rc.clone())
            }
            };
        if let Some(e) = e { // New item found in tree trasversal
            todo!("found element");
            println!("e: {}", e.borrow().val);
        }
    }
}
